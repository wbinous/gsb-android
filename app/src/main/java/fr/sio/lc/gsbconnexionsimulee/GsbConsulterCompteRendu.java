package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import fr.sio.lc.gsbconnexionsimulee.entites.Praticien;
import fr.sio.lc.gsbconnexionsimulee.entites.Visiteur;
import fr.sio.lc.gsbconnexionsimulee.modeles.ModeleGsb;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class GsbConsulterCompteRendu extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private List<Praticien> lesPraticiens = new ArrayList<Praticien>();
    private ListView lvLesPraticiens;
    private TextView listeVide;
    private Visiteur leVisiteur;
    Spinner spin;
    List<String> array ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsb_consulter_compte_rendu);

        TextView visiteur = (TextView) findViewById(R.id.idVisiteur);
        leVisiteur = Session.getSession().getLeVisiteur();
        String nom = Session.getSession().getLeVisiteur().getVisNom();
        String prenom = Session.getSession().getLeVisiteur().getVisPrenom();
        String matricule = Session.getSession().getLeVisiteur().getVisMatricule();

        visiteur.setText(prenom +" "+ nom);

        String url = "http://192.168.1.38/gsb_Android/web/app_dev.php/getUneDate/"+ leVisiteur.getVisMatricule();


        final Response.Listener<JSONArray> responseListener  = new Response.Listener<JSONArray>() {
            @Override public void onResponse(JSONArray response) {
                try {
                    for( int i= 0 ; i < response.length() ; i++ )
                    { Log.i( "APP-RV" , response.getJSONObject( i ).getString("rapDatevisite")) ;
                            //array.add(response.getJSONObject(i).getString("rapDatevisite"));
                    }
                    //spin = findViewById(R.id.spinner);
                    //ArrayAdapter<String> adapter = new ArrayAdapter<String>(GsbConsulterCompteRendu.this,android.R.layout.simple_spinner_item,array);
                    //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    //spin.setAdapter(adapter);
                } catch(JSONException e){
                    Log.e( "APP-RV" , "Erreur JSON : " + e.getMessage() ) ;
                }
            }
        } ;
        Response.ErrorListener ecouteurErreur = new Response.ErrorListener(){
            @Override public void onErrorResponse(VolleyError error) { Log.e( "APP-RV" , "Erreur HTTP : " + error.getMessage() ) ;
            }
        } ; JsonArrayRequest requete = new JsonArrayRequest( Request.Method.GET , url , null , responseListener , ecouteurErreur ) ;
        RequestQueue fileReq = Volley.newRequestQueue( this) ;
        fileReq.add( requete ) ;


        /*if (ModeleGsb.getLesPraticiens(leVisiteur).size() == 0) {
            listeVide = (TextView) findViewById(R.id.idListeVide);
            listeVide.setText("Liste vide");
        } else {
            lvLesPraticiens = (ListView) findViewById(R.id.idLesPraticiens);
            ItemPraticienAdapter adapter = new ItemPraticienAdapter(this,
                    ModeleGsb.getInstance().getLesPraticiens(leVisiteur));

            lvLesPraticiens.setAdapter(adapter);
            lvLesPraticiens.setOnItemClickListener(this);

        }*/

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View vue, int position, long id) {
        /* Praticien praticienSelectionne = ModeleGsb.getInstance().getLesPraticiens(leVisiteur).get(position);

        Intent detailPraticien = new Intent(this, GsbUnPraticienActivity.class);
        detailPraticien.putExtra("praticien", praticienSelectionne);

        startActivity(detailPraticien);*/
    }

    public void revenirMenu(View vue){

        Intent retourMenu = new Intent(this, GsbMenuActivity.class);
        startActivity(retourMenu);
    }
}
