package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;

import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class GsbMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsb_menu);

        Intent connexion = getIntent();

        TextView visiteur = (TextView) findViewById(R.id.idVisiteur);
        String nom = Session.getSession().getLeVisiteur().getVisNom();
        String prenom = Session.getSession().getLeVisiteur().getVisPrenom();
        visiteur.setText(prenom +" "+ nom);
    }

    public void consulterCompteRendu(View vue) {
        Intent saisirCR = new Intent(this, GsbConsulterCompteRendu.class);
        startActivity(saisirCR);
    }

    public void saisirCompteRendu(View vue) {
        Intent consulterCR = new Intent(this, GsbSaisirCompteRendu.class);
        startActivity(consulterCR);
    }

    public void seDeconnecter(View vue) {
        Session.fermer();

        Intent retourAccueil = new Intent(this, MainActivity.class);
        startActivity(retourAccueil);
    }

}
