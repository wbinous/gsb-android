package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;


import fr.sio.lc.gsbconnexionsimulee.entites.Visiteur;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class MainActivity extends AppCompatActivity {

    public static final String TAG="MainActivity";
    EditText edtLogin;
    EditText edtMdp;
    Button btnSeConnecter;
    Button btnAnnuler;
    String nomVisiteur ;
    Visiteur visiteur;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //visiteur = new Visiteur();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtLogin = (EditText) findViewById(R.id.idLogin);
        edtMdp = (EditText) findViewById(R.id.idMdp);

        btnSeConnecter = (Button) findViewById(R.id.idSeConnecter);
        //btnSeConnecter.setOnClickListener((View.OnClickListener) this);
        btnAnnuler = (Button) findViewById(R.id.bAnnulerCo);
        //btnAnnuler.setOnClickListener((View.OnClickListener) this);
    }

    public void seConnecterEnLocal(View view) {
        String login = edtLogin.getText().toString();
        String mdp = edtMdp.getText().toString();

        connecterVisiteur("a17", login,"Andre");
    }

    public void seConnecter(View view) {
        // Action sur clic du bouton Se connecter
        String login = edtLogin.getText().toString();
        String mdp = edtMdp.getText().toString();

        String url = "http://192.168.1.38/gsb_Android/web/app_dev.php/connexion/" + login + "/" + mdp;

        Response.Listener<JSONObject> responseListener = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("APP_RV","Visiteur : "+ response.toString());
                try {
                    String matricule = response.getString("visMatricule");
                    String nom = response.getString("visNom");
                    String prenom = response.getString("visPrenom");
                    Toast.makeText(MainActivity.this, "Connexion réussie : "
                            + matricule, Toast.LENGTH_LONG).show();
                    Log.i("APP_RV","Visiteur : "+ response.toString());
                    connecterVisiteur(matricule, nom, prenom);
                } catch (JSONException e) {
                    if (Session.getSession() != null) {
                        Session.getSession().fermer();
                    }
                    Toast.makeText(MainActivity.this, "Echec de connexion ",
                            Toast.LENGTH_LONG).show();
                    Log.e("APP-RV", "Erreur : " + e.getMessage());
                }
            }
        };

        Response.ErrorListener responseErrorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("APP_RV", "Erreur JSON : " + error.getMessage());
            }
        };

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                responseListener, responseErrorListener);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonObjectRequest);

        //System.out.println(visiteur.toString());
    }

    public void connecterVisiteur(String matricule, String nom, String prenom) {

        visiteur = new Visiteur();
        visiteur.setVisMatricule(matricule);
        visiteur.setVisNom(nom);
        visiteur.setVisPrenom(prenom);
        if ((Session.init(visiteur)) == false) {
            Log.e("Main activity","Erreur session");
        } else {
            Intent intentMenu = new Intent(this, GsbMenuActivity.class);
            startActivity(intentMenu);
        }

    }
    public void reinitialiser(View view) {

        // Action sur clic du bouton Annuler
        edtLogin.setText("");
        edtMdp.setText("");
        if (Session.getSession() != null) {
            Session.getSession().fermer();
        }
    }
}
