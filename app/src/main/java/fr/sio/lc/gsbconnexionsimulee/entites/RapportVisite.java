package fr.sio.lc.gsbconnexionsimulee.entites;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.os.Parcel;
import android.os.Parcelable;

public class RapportVisite implements Parcelable {

	private static final long serialVersion = 1L;
	
	private int numero ;
	private String bilan ;
	private GregorianCalendar dateVisite ;
	private GregorianCalendar dateRedaction ;
	
	private fr.sio.lc.gsbconnexionsimulee.entites.Praticien lePraticien ;
	private fr.sio.lc.gsbconnexionsimulee.entites.Visiteur leVisiteur ;
	
	public static final Parcelable.Creator<RapportVisite> CREATOR = new Parcelable.Creator<RapportVisite>() {

		@Override
		public RapportVisite createFromParcel(Parcel source) {
			// TODO Auto-generated method stub
			return new RapportVisite(source);
		}

		@Override
		public RapportVisite[] newArray(int size) {
			// TODO Auto-generated method stub
			return new RapportVisite[size];
		}
		
	};
	
	public RapportVisite() {
		super();
	}
	
	protected RapportVisite(Parcel in){
		this.numero = in.readInt();
		this.bilan = in.readString();
		this.lePraticien = new Praticien();
		this.leVisiteur = new Visiteur();
		
	}
	
	public RapportVisite(int numero, String bilan, GregorianCalendar dateVisite, GregorianCalendar dateRedaction) {
		super();
		this.numero = numero;
		this.bilan = bilan;
		this.dateVisite = dateVisite;
		this.dateRedaction = dateRedaction;
	}

	public RapportVisite(int numero, String bilan, GregorianCalendar dateVisite, GregorianCalendar dateRedaction,
			Praticien lePraticien, Visiteur leVisiteur) {
		super();
		this.numero = numero;
		this.bilan = bilan;
		this.dateVisite = dateVisite;
		this.dateRedaction = dateRedaction;
		this.lePraticien = lePraticien;
		this.leVisiteur = leVisiteur;
	}

	
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getBilan() {
		return bilan;
	}

	public void setBilan(String bilan) {
		this.bilan = bilan;
	}

	public GregorianCalendar getDateVisite() {
		return dateVisite;
	}

	public void setDateVisite(GregorianCalendar dateVisite) {
		this.dateVisite = dateVisite;
	}

	public GregorianCalendar getDateRedaction() {
		return dateRedaction;
	}

	public void setDateRedaction(GregorianCalendar dateRedaction) {
		this.dateRedaction = dateRedaction;
	}

	public Praticien getLePraticien() {
		return lePraticien;
	}

	public void setLePraticien(Praticien lePraticien) {
		this.lePraticien = lePraticien;
	}

	public Visiteur getLeVisiteur() {
		return leVisiteur;
	}

	public void setLeVisiteur(Visiteur leVisiteur) {
		this.leVisiteur = leVisiteur;
	}
	
	// nouvelle m�thode toString() qui ne renvoie que le l'essentiel
	// pour un meilleur affichage des listes
	
	public String toString(){
		return "le " + this.dateVisite.get(Calendar.DAY_OF_MONTH) + " : " 
				+ this.lePraticien.getPraPrenom() + " " + this.lePraticien.getPraNom();
	}
	
	
	
	
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public String decrire() {
		return "RapportVisite [numero=" + numero + ", bilan=" + bilan
				+ ", dateVisite=" + dateVisite + ", dateRedaction=" + dateRedaction
				+ ", lePraticien=" + lePraticien + ", leVisiteur="
				+ leVisiteur + "]";
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		dest.writeString(this.bilan);
		//dest.writeParcelable((Parcelable) this.dateVisite, flags);
		//dest.writeInt(this.dateVisite.get(Calendar.DAY_OF_MONTH));
		//dest.writeInt(this.dateVisite.get(Calendar.MONTH));
		//dest.writeInt(this.dateVisite.get(Calendar.YEAR));

		dest.writeParcelable((Parcelable) this.lePraticien, flags);
		
		dest.writeInt(this.lePraticien.getPraNum());
		
		dest.writeInt(this.numero);
	}
	
}
