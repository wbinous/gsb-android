package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.sio.lc.gsbconnexionsimulee.entites.RapportVisite;
import fr.sio.lc.gsbconnexionsimulee.entites.Visiteur;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class GsbSaisirCompteRendu extends AppCompatActivity{

    private Visiteur leVisiteur;
    Spinner spin;
    List<String>array;
    /*GsonBuilder fabrique = new GsonBuilder() ;

    final RapportVisite lerapportVisite;
    final Gson gson = fabrique.create() ;

    public GsbSaisirCompteRendu(RapportVisite lerapportVisite) {
        this.lerapportVisite = lerapportVisite;
    }*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsb_saisir_compte_rendu);

        TextView visiteur = (TextView) findViewById(R.id.idVisiteur);
        leVisiteur = Session.getSession().getLeVisiteur();
        String nom = Session.getSession().getLeVisiteur().getVisNom();
        String prenom = Session.getSession().getLeVisiteur().getVisPrenom();
        String matricule = Session.getSession().getLeVisiteur().getVisMatricule();

        visiteur.setText(prenom + " " + nom);
        String url = "http://192.168.1.38/gsb_Android/web/app_dev.php/listePraticien";

        final Response.Listener<JSONArray> responseListener  = new Response.Listener<JSONArray>() {
            @Override public void onResponse(JSONArray response) {
                try {
                    for( int i = 0 ; i < response.length() ; i++ ) {
                        Log.i( "APP-RV" , response.getJSONArray( i ).getString(i)) ;
                           // array.add(response.getJSONObject(i).getString(""));

                    }

                  /*  spin = findViewById(R.id.spinner2);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(GsbSaisirCompteRendu.this,android.R.layout.simple_spinner_item, array);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spin.setAdapter(adapter);*/
                } catch(JSONException e){
                    Log.e( "APP-RV" , "Erreur JSON : " + e.getMessage() ) ;
                }
            }

        } ;
        Response.ErrorListener ecouteurErreur = new Response.ErrorListener(){
            @Override public void onErrorResponse(VolleyError error) { Log.e( "APP-RV" , "Erreur HTTP : " + error.getMessage() ) ;
            }
        } ; JsonArrayRequest requete = new JsonArrayRequest( Request.Method.GET , url , null , responseListener , ecouteurErreur ) ;
        RequestQueue fileReq = Volley.newRequestQueue( this) ;
        fileReq.add( requete ) ;




        String url2 ="http://192.168.1.38/gsb_Android/web/app_dev.php/listePraticien"; ;

         /*
            try { StringRequest requete2 = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {

                @Override public void onResponse(String response) {

                }

                } , new Response.ErrorListener(){
                @Override public void onErrorResponse(VolleyError error) {

                }
            } ){
                @Override protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> parametres = new HashMap<String,String>() ; parametres.put( "ville" , gson.toJson( lerapportVisite ) )
                    ; return parametres ;
                }
            } ;
            RequestQueue fileRequete = Volley.newRequestQueue( this ) ;
            fileRequete.add( requete2 ) ;
            } catch( Exception e ){
                Log.e( "APP-RV" , e.getMessage() ) ;
            }

    */
    }

    public void revenirMenu(View vue){

        Intent retourMenu = new Intent(this, GsbMenuActivity.class);
        startActivity(retourMenu);

    }

}
