package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import fr.sio.lc.gsbconnexionsimulee.entites.Praticien;
import fr.sio.lc.gsbconnexionsimulee.entites.Visiteur;
import fr.sio.lc.gsbconnexionsimulee.modeles.ModeleGsb;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class GsbLesPraticiensActivity extends AppCompatActivity {
    private List<Praticien> lesPraticiens = new ArrayList<Praticien>();
    private ListView lvLesPraticiens;
    private TextView listeVide;
    private Visiteur leVisiteur;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsb_les_praticiens);

        TextView visiteur = (TextView) findViewById(R.id.idVisiteur);
        leVisiteur = Session.getSession().getLeVisiteur();
        String nom = Session.getSession().getLeVisiteur().getVisNom();
        String prenom = Session.getSession().getLeVisiteur().getVisPrenom();
        String matricule = Session.getSession().getLeVisiteur().getVisMatricule();

        visiteur.setText(prenom +" "+ nom);


        /* if (ModeleGsb.getLesPraticiens(leVisiteur).size() == 0) {
            listeVide = (TextView) findViewById(R.id.idListeVide);
            listeVide.setText("Liste vide");
        } else {
            lvLesPraticiens = (ListView) findViewById(R.id.idLesPraticiens);
            ItemPraticienAdapter adapter = new ItemPraticienAdapter(this,
                    ModeleGsb.getLesPraticiens(leVisiteur));

            lvLesPraticiens.setAdapter(adapter);
            // lvLesPraticiens.setOnItemClickListener(this);

        }*/

    }

    /*@Override
    public void onItemClick(AdapterView<?> parent, View vue, int position, long id) {
        Praticien praticienSelectionne = ModeleGsb.getInstance().getLesPraticiens(leVisiteur).get(position);

        Intent detailPraticien = new Intent(this, GsbUnPraticienActivity.class);
        detailPraticien.putExtra("praticien", praticienSelectionne);

        startActivity(detailPraticien);
    }*/

    public void revenirMenu(View vue){

        Intent retourMenu = new Intent(this, GsbMenuActivity.class);
        startActivity(retourMenu);
    }
}
