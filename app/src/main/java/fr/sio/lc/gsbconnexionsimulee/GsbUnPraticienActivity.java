package fr.sio.lc.gsbconnexionsimulee;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import fr.sio.lc.gsbconnexionsimulee.entites.Praticien;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

public class GsbUnPraticienActivity extends AppCompatActivity {
    private TextView numeroPraticien;
    private TextView nomPraticien;
    private TextView prenomPraticien;
    private TextView adressePraticien;
    private TextView cpPraticien;
    private TextView villePraticien;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gsb_un_praticien);

        TextView visiteur = (TextView) findViewById(R.id.idVisiteur);
        String nom = Session.getSession().getLeVisiteur().getVisNom();
        String prenom = Session.getSession().getLeVisiteur().getVisPrenom();
        String matricule = Session.getSession().getLeVisiteur().getVisMatricule();

        visiteur.setText(prenom +" "+ nom);

        numeroPraticien = (TextView) findViewById(R.id.idNumPraticien);
        nomPraticien = (TextView) findViewById(R.id.idNomPraticien);
        prenomPraticien = (TextView) findViewById(R.id.idPrenomPraticien);
        
        Praticien praticien = this.getIntent().getParcelableExtra("praticien");

        numeroPraticien.setText(Integer.toString(praticien.getPraNum()));
        nomPraticien.setText(praticien.getPraNom());
        prenomPraticien.setText(praticien.getPraPrenom());

    }

    public void revenirMenu(View vue){

        Intent retourMenu = new Intent(this, GsbMenuActivity.class);
        startActivity(retourMenu);
    }
}
