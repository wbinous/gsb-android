package fr.sio.lc.gsbconnexionsimulee;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import fr.sio.lc.gsbconnexionsimulee.entites.Praticien;
import fr.sio.lc.gsbconnexionsimulee.modeles.ModeleGsb;
import fr.sio.lc.gsbconnexionsimulee.techniques.Session;

/**
 * Created by AH on 28/04/2018.
 */

public class ItemPraticienAdapter extends ArrayAdapter<Praticien> {
    public ItemPraticienAdapter (Context context, List<Praticien> lesPraticiens) {
        super (context, -1, lesPraticiens);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View vItem = null;

        if (convertView != null) {
            vItem = convertView;
        } else {
            vItem = layoutInflater.inflate(R.layout.item_praticien, parent, false);
        }

        /* TextView tvNumPraticien = (TextView) vItem.findViewById(R.id.idNumPraticien);
        tvNumPraticien.setText(Integer.toString(ModeleGsb.getLesPraticiens(Session.getSession().getLeVisiteur()).get(position).getNumero()));
        //tvNumPraticien.setText(Integer.toString(this.lesPraticiens.get(position).getNumero()));

        TextView tvNomPraticien = (TextView) vItem.findViewById(R.id.idNomPraticien);
        tvNomPraticien.setText(ModeleGsb.getLesPraticiens(Session.getSession().getLeVisiteur()).get(position).getNom());

        TextView tvPrenomPraticien = (TextView) vItem.findViewById(R.id.idPrenomPraticien);
        tvPrenomPraticien.setText(ModeleGsb.getLesPraticiens(Session.getSession().getLeVisiteur()).get(position).getPrenom());
    */
        return vItem;
    }
}
