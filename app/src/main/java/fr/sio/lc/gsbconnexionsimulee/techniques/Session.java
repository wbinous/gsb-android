package fr.sio.lc.gsbconnexionsimulee.techniques;

import fr.sio.lc.gsbconnexionsimulee.entites.Visiteur;
import fr.sio.lc.gsbconnexionsimulee.modeles.ModeleGsb;

public class Session {

	private static Session session = null ;
	private Visiteur leVisiteur ;
	
	private Session(Visiteur leVisiteur){
		super() ;
		this.leVisiteur = leVisiteur ;
	}

	public static boolean init(Visiteur visiteur)  {
		boolean res = false;
		System.out.println("init");
		if (visiteur != null) {
			Session.session = new Session(visiteur);
			return true;
		} else {
			return false;
		}
		//return res;
	}
	
	public static Session getSession(){
		return Session.session ;
	}
	
	public static void fermer(){
		Session.session = null ;
	}
	
	public Visiteur getLeVisiteur(){
		return this.leVisiteur ;
	}
}
